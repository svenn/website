import { createRouter, createWebHistory } from 'vue-router'
import Home from '/src/pages/Home.vue'
import Projects from '/src/pages/Projects.vue'

const routes = [
    {
        path: '/',
        name: 'About me',
        component: Home,
    },
    {
        path: '/projects',
        name: 'Projects',
        component: Projects,
    },
]
const router = createRouter({
    history: createWebHistory(),
    routes,
})
export default router